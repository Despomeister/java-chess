public class Pionek
{

    String pole_znak;
    String pole_cyfra;
    String nazwa_figury;
    String kolor;

    public Pionek(String pole_znak, String pole_cyfra, String nazwa_figury, String kolor) {
        this.pole_znak = pole_znak;
        this.pole_cyfra = pole_cyfra;
        this.nazwa_figury = nazwa_figury;
        this.kolor = kolor;
    }

    public String getPole_znak() {
        return pole_znak;
    }

    public void setPole_znak(String pole_znak) {
        this.pole_znak = pole_znak;
    }

    public String getPole_cyfra() {
        return pole_cyfra;
    }

    public void setPole_cyfra(String pole_cyfra) {
        this.pole_cyfra = pole_cyfra;
    }

    public String getNazwa_figury() {
        return nazwa_figury;
    }

    public void setNazwa_figury(String nazwa_figury) {
        this.nazwa_figury = nazwa_figury;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }
}
