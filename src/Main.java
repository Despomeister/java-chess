import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main {

    //Dane
    public static Pionek [][] szachownica;
    static int y = 8;
    public static Map<Integer,String> znaki;
    public static ArrayList<Integer> cyfry;
    public static int[] znaki_ascii = {65,66,67,68,69,70,71,72};
    public static String[] kolory = {"Bialy", "Czarny"};
    public static Pionek[][] elelemnty_zczytane_z_pliku;
    public static String filename = "D:\\Studia\\Semestr VIII\\Java\\Szachy\\plik_wejsciowy.txt";
    public static String figura_na_podanym_polu = "";
    public static String podane_pole_figury = "";
    public static int wiersz = 0;
    public static int kolumna = 0;
    public static int kolumna_docelowa = 0;
    public static int wiersz_docelowy = 0;
    public static boolean jest_figura = false;
    public static String nazwa_figury = "";
    public static String kolor_figury = "";
    public static int kolumna_figury = 0;
    public static int wiersz_figury = 0;
    public static boolean Czy_jest_szach = false;
    public static boolean Czy_mozna_wykonac_ruch = false;

    public static void main(String[] args) throws IOException {

        //Inicjalizacja
        szachownica = new Pionek[8][8];
        elelemnty_zczytane_z_pliku = new Pionek[8][8];
        znaki = new HashMap<>();
        cyfry = new ArrayList<>();

        //Uzupelnianie Tablic
        for (int i = 0; i < 8; i++) znaki.put(i + 1, Character.toString((char) znaki_ascii[i]));
        for (int i = 8; i > 0; i--) cyfry.add(i);

        //Legenda
        System.out.println("\nProgram sprawdza czy na aktualnie zaladowanej szachownicy z pliku, mozliwe jest wykonanie ruchu" +
                " podanego przez uzytkownika.");
        System.out.println("Oznaczenia figur: ");
        System.out.println("K - Krol");
        System.out.println("Q - Krolowa");
        System.out.println("G - Goniec");
        System.out.println("S - Skoczek");
        System.out.println("W - Wieza");
        System.out.println("P - Piony");
        System.out.println("Gorna czesc szachownicy zajmuja pionki o kolorze czarnym, natomiast dolna czesc pionki o kolorze bialym.");

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                szachownica[i][j] = new Pionek("" + znaki.get(j + 1), "" + cyfry.get(i), "W", kolory[0]);
            }
        }

        //Wczytywanie danych z pliku
        int i = 1;
        int j = 1;

        StringBuilder figura = new StringBuilder();
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(filename),
                        StandardCharsets.UTF_8));
        int c;
        while (i != 9) {
            while ((c = reader.read()) != -1) {
                char character = (char) c;
                if (character != ',') {
                    figura.append(character);
                } else {
                    String temp = figura + " ";

                    //Ustawianie nazwy figury na aktualnym polu, jesli nie ma to jest to spacja
                    szachownica[i - 1][j - 1].setNazwa_figury(figura + "");

                    //Ustawianie koloru figur na podstawie danych zczytanych z pliku
                    if ((temp.charAt(0) + "").equals("B"))
                        szachownica[i - 1][j - 1].setKolor("Bialy");
                    else if ((temp.charAt(0) + "").equals("C"))
                        szachownica[i - 1][j - 1].setKolor("Czarny");
                    else
                        szachownica[i - 1][j - 1].setKolor("");

                    figura = new StringBuilder();
                    j++;
                }

                if (j == 9) {
                    j = 1;
                    i++;
                }
            }
        }

        //Rysowanie szachwonicy
        System.out.println("\nAktualnie wczytana szachwonica z pliku: \n");
        Rysowanie_szachownicy();

        //Petla wykonuje sie dopoki nie wybierzemy niepustego pola na planszy
        while (!jest_figura) {
            System.out.print("\nPodaj pole figury, ktora chcesz wykonac ruch (Na przyklad A8): ");
            Scanner scan = new Scanner(System.in);
            podane_pole_figury = scan.nextLine();
            podane_pole_figury = podane_pole_figury.toUpperCase();
            System.out.println("\nPodane przez ciebie pole to: " + podane_pole_figury);
            Znajdz_docelowe_pole(podane_pole_figury);
            kolumna_figury = kolumna;
            wiersz_figury = wiersz;

            //Sprawdzanie koloru figury
            if ((figura_na_podanym_polu.charAt(0) + "").equals("C")) {
                System.out.println("Kolor figury na tym polu: Czarny");
                kolor_figury = "Czarny";
                jest_figura = true;
            } else if ((figura_na_podanym_polu.charAt(0) + "").equals("B")) {
                System.out.println("Kolor figury na tym polu: Bialy");
                kolor_figury = "Bialy";
                jest_figura = true;
            } else {
                System.out.println("Na podanym polu nie ma zadnej figury");
            }
        }

        //Sprawdzanie nazwy figury
        switch ((figura_na_podanym_polu.charAt(1) + "")) {
            case "K":
                System.out.println("Nazwa figury na tym polu: Krol");
                nazwa_figury = "Krol";
                break;
            case "Q":
                System.out.println("Nazwa figury na tym polu: Krolowa");
                nazwa_figury = "Krolowa";
                break;
            case "G":
                System.out.println("Nazwa figury na tym polu: Goniec");
                nazwa_figury = "Goniec";
                break;
            case "S":
                System.out.println("Nazwa figury na tym polu: Skoczek");
                nazwa_figury = "Skoczek";
                break;
            case "W":
                System.out.println("Nazwa figury na tym polu: Wieza");
                nazwa_figury = "Wieza";
                break;
            case "P":
                System.out.println("Nazwa figury na tym polu: Pionek");
                nazwa_figury = "Pionek";
                break;
        }

        //Warunki dla pionkow
        if (nazwa_figury.equals("Pionek")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_pionek();
        }

        //Warunki dla wiezy bialej
        if (nazwa_figury.equals("Wieza") && kolor_figury.equals("Bialy")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_wieza("Czarny");
        }
        //Warunki dla wiezy czarnej
        else if (nazwa_figury.equals("Wieza")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_wieza("Bialy");
        }

        //Warunki dla bialych skoczkow
        if (nazwa_figury.equals("Skoczek") && kolor_figury.equals("Bialy")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_skoczek("Czarny");
        }
        //Warunki dla czarnych skoczkow
        else if (nazwa_figury.equals("Skoczek")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_skoczek("Bialy");
        }

        //Warunki dla bialych goncow
        if (nazwa_figury.equals("Goniec") && kolor_figury.equals("Bialy")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_goniec("Czarny");
        }
        //Warunki dla czarnych goncow
        else if (nazwa_figury.equals("Goniec")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_goniec("Bialy");
        }

        //Warunki dla bialej krolowej
        if (nazwa_figury.equals("Krolowa") && kolor_figury.equals("Bialy")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_krolowa("Czarny");
        }
        //Warunki dla czarnej krolowej
        else if (nazwa_figury.equals("Krolowa")) {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_krolowa("Bialy");
        }

        //Warunki dla bialego krola
        if (nazwa_figury.equals("Krol") && kolor_figury.equals("Bialy"))
        {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_krol("Czarny");
        }
        //Warunki dla czarnego krola
        else if (nazwa_figury.equals("Krol"))
        {
            String docelowe_pole = Podaj_pole();
            Znajdz_docelowe_pole(docelowe_pole);
            kolumna_docelowa = kolumna;
            wiersz_docelowy = wiersz;
            Warunek_krol("Bialy");
        }

        if(Czy_mozna_wykonac_ruch)
            Czy_krol_nie_bedzie_szachowany();
    }

    //Metoda rysujaca szachownice
    public static void Rysowanie_szachownicy()
    {
        System.out.println("    A   B   C   D   E   F   G   H");
        System.out.println("  ---------------------------------");
        for (int i = 0; i < 8; i ++)
        {
            System.out.print(y);
            for (int j = 0; j < 8; j ++)
            {
                if((szachownica[i][j].getNazwa_figury().charAt(0)+"").equals(" "))
                    System.out.print(" |  ");
                else
                    System.out.print(" | " + szachownica[i][j].getNazwa_figury().charAt(1));

            }
            System.out.print(" | "+(y) + "\n");
            System.out.println("  ---------------------------------");
            y--;
        }
        System.out.println("    A   B   C   D   E   F   G   H");
        y=8;
    }

    //Metoda zczytujaca pole naszej figur
    public static String Podaj_pole()
    {
        System.out.print("\nPodaj pole,na ktore chcesz wykonac ruch (Na przyklad A8): ");
        Scanner scan = new Scanner(System.in);
        String podane_pole_figury = scan.nextLine();
        podane_pole_figury = podane_pole_figury.toUpperCase();
        System.out.println("\nPodane przez ciebie pole to: " + podane_pole_figury);
        return podane_pole_figury;
    }

    //Metoda przypisujaca indeksy naszego pola (wiersz, kolumna)
    public static void Znajdz_docelowe_pole(String docelowe_pole)
    {
        for (int k = 0; k < 8; k++)
            for (int l = 0; l < 8; l++)
                if (((docelowe_pole.charAt(0) + "").equals(szachownica[k][l].getPole_znak())) && ((docelowe_pole.charAt(1) + "").equals(szachownica[k][l].getPole_cyfra()))) {
                    figura_na_podanym_polu = szachownica[k][l].getNazwa_figury();
                    wiersz = k;
                    kolumna = l;
                }
    }

    //Metoda sprawdzajaca mozliwosci ruchu dla pionka
    private static void Warunek_pionek()
    {
        //Warunki dla bialych
        //Ruch gora
        System.out.println("\nSprawdzamy warunek dla pionka:");
        if(((kolumna_docelowa - kolumna_figury) == 0) && ((wiersz_docelowy - wiersz_figury) == -1) && (szachownica[wiersz_figury][kolumna_figury].getKolor().equals("Bialy")))
        {
            if (szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) {
                System.out.println("Mozna wykonac ruch - Podane pole jest puste.");
                Czy_mozna_wykonac_ruch = true;
            }
            else System.out.println("Nie mozna wykonac ruchu - Podane pole jest zajete przez inny pionek");
        }
        //Ruch prawy gorny ukos - bicie
        else if(((kolumna_docelowa - kolumna_figury) == 1) && ((wiersz_docelowy - wiersz_figury) == -1) && (szachownica[wiersz_figury][kolumna_figury].getKolor().equals("Bialy")))
        {
            if((!szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) && (szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals("Czarny"))) {
                System.out.println("Mozna wykonac ruch - Mamy bicie!");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else System.out.println("Nie mozna wykonac ruchu - Podane pole jest puste");
        }
        //Ruch lewy gorny ukos
        else if(((kolumna_docelowa - kolumna_figury) == -1) && ((wiersz_docelowy - wiersz_figury) == -1) && (szachownica[wiersz_figury][kolumna_figury].getKolor().equals("Bialy")))
        {
            if((!szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) && (szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals("Czarny"))) {
                System.out.println("Mozna wykonac ruch - Mamy bicie!");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else System.out.println("Nie mozna wykonac ruchu - Podane pole jest puste");
        }
        //Warunki dla czarnych
        //Ruch dol
        else if(((kolumna_docelowa - kolumna_figury) == 0) && ((wiersz_docelowy - wiersz_figury) == 1) && (szachownica[wiersz_figury][kolumna_figury].getKolor().equals("Czarny")))
        {
            if (szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) {
                System.out.println("Mozna wykonac ruch - Podane pole jest puste.");
                Czy_mozna_wykonac_ruch = true;
            }
            else System.out.println("Nie mozna wykonac ruchu - Podane pole jest zajete przez inny pionek");
        }
        //Ruch prawy dolny ukos
        else if(((kolumna_docelowa - kolumna_figury) == 1) && ((wiersz_docelowy - wiersz_figury) == 1) && (szachownica[wiersz_figury][kolumna_figury].getKolor().equals("Czarny")))
        {
            if((!szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) && (szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals("Bialy"))) {
                System.out.println("Mozna wykonac ruch - Mamy bicie!");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else System.out.println("Nie mozna wykonac ruchu - Podane pole jest puste");
        }
        //Ruch lewy dolny ukos
        else if(((kolumna_docelowa - kolumna_figury) == -1) && ((wiersz_docelowy - wiersz_figury) == 1) && (szachownica[wiersz_figury][kolumna_figury].getKolor().equals("Czarny")))
        {
            if((!szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) && (szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals("Bialy"))) {
                System.out.println("Mozna wykonac ruch - Mamy bicie!");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else System.out.println("Nie mozna wykonac ruchu - Podane pole jest puste");
        }
        else System.out.println("Pionek nie moze wykonac takiego ruchu!");
    }

    //Metoda sprawdzajaca mozliwosci ruchu dla wiezy
    public static void Warunek_wieza(String kolor)
    {
        //Ruch po osi x
        if((szachownica[wiersz_figury][kolumna_figury].getNazwa_figury().charAt(1)+"").equals("W"))
            System.out.println("\nSprawdzamy warunek dla wiezy:");
        if (((kolumna_figury - kolumna_docelowa) != 0) && (wiersz_figury - wiersz_docelowy) == 0) {
            System.out.println("Przesuwamy sie po x");
            System.out.println("Ilosc pol do przejscia: " + (Math.abs(kolumna_figury - kolumna_docelowa)));
            int ilosc_pol_do_przejscia = kolumna_docelowa - kolumna_figury;
            boolean flaga = false;

            //Idziemy w lewo
            if (ilosc_pol_do_przejscia > 0)
                for (int w = 1; w <= ilosc_pol_do_przejscia; w++) {
                    if (szachownica[wiersz_figury][kolumna_figury + (w)].getNazwa_figury().equals(" ")) {
                        flaga = true;
                    } else {
                        if ((Math.abs(ilosc_pol_do_przejscia) - w) == 0)
                            if(szachownica[wiersz_figury][kolumna_docelowa].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if ((Math.abs(ilosc_pol_do_przejscia) - w) > 0) {
                            flaga = false;
                            break;
                        }
                    }
                }
                //Idziemy w prawo
            else if (ilosc_pol_do_przejscia < 0)
                for (int w = 1; w <= Math.abs(ilosc_pol_do_przejscia); w++) {
                    if (szachownica[wiersz_figury][kolumna_figury - (w)].getNazwa_figury().equals(" ")) {
                        flaga = true;
                    } else {
                        if ((Math.abs(ilosc_pol_do_przejscia) - w) == 0)
                            if(szachownica[wiersz_figury][kolumna_docelowa].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if ((Math.abs(ilosc_pol_do_przejscia) - w) > 0) {
                            flaga = false;
                            break;
                        }
                    }
                }
            if (flaga) {
                System.out.println("Mozna wykonac ruch");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else
                System.out.println("Nie mozna wykonac ruch!");
        }
        //Ruch po osi y
        else if (((wiersz_figury - wiersz_docelowy) != 0) && (kolumna_figury - kolumna_docelowa) == 0) {
            System.out.println("Przesuwamy sie po y");
            System.out.println("Ilosc pol do przejscia: " + (Math.abs(wiersz_figury - wiersz_docelowy)));
            int ilosc_pol_do_przejscia = wiersz_docelowy - wiersz_figury;
            boolean flaga = false;

            //Idziemy w dol
            if (ilosc_pol_do_przejscia > 0)
                for (int w = 1; w <= ilosc_pol_do_przejscia; w++) {
                    if (szachownica[wiersz_figury + (w)][kolumna_figury].getNazwa_figury().equals(" ")) {
                        flaga = true;
                    } else {
                        if ((Math.abs(ilosc_pol_do_przejscia) - w) == 0)
                            if(szachownica[wiersz_docelowy][kolumna_figury].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if ((Math.abs(ilosc_pol_do_przejscia) - w) > 0) {
                            flaga = false;
                            break;
                        }
                    }
                }
                //Idziemy w gore
            else if (ilosc_pol_do_przejscia < 0)
                for (int w = 1; w <= Math.abs(ilosc_pol_do_przejscia); w++) {
                    if (szachownica[wiersz_figury - (w)][kolumna_figury].getNazwa_figury().equals(" ")) {
                        flaga = true;
                    } else {
                        if ((Math.abs(ilosc_pol_do_przejscia) - w) == 0) {
                            if(szachownica[wiersz_docelowy][kolumna_figury].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        } else if ((Math.abs(ilosc_pol_do_przejscia) - w) > 0) {
                            flaga = false;
                            break;
                        }
                    }
                }
            if (flaga) {
                System.out.println("Mozna wykonac ruch");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else
                System.out.println("Nie mozna wykonac ruch!");

        } else
            System.out.println("Wieza nie da sie wykonac bezposredniego ruchu na to pole!");
    }

    //Metoda sprawdzajaca mozliwosci ruchu dla skoczka
    private static void Warunek_skoczek(String kolor)
    {
        System.out.println("\nSprawdzamy warunek dla skoczka:");
        if (((kolumna_docelowa - kolumna_figury) == 2) && (wiersz_docelowy - wiersz_figury) == -1) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == 2) && (wiersz_docelowy - wiersz_figury) == 1) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == 1) && (wiersz_docelowy - wiersz_figury) == -2) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == 1) && (wiersz_docelowy - wiersz_figury) == 2) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == -1) && (wiersz_docelowy - wiersz_figury) == -2) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == -1) && (wiersz_docelowy - wiersz_figury) == 2) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == -2) && (wiersz_docelowy - wiersz_figury) == -1) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else if (((kolumna_docelowa - kolumna_figury) == -2) && (wiersz_docelowy - wiersz_figury) == 1) {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else System.out.println("Nie mozna wykonac ruchu - Skoczek nie moze wykonac takiego ruchu");
    }

    //Metoda sprawdzajaca czy na docelowe pole mozna wykonac ruch
    private static void Czy_mozna_wykonac_ruch(String kolor)
    {
        if (szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) {
            System.out.println("Mozna wykonac ruch - Podane pole jest puste.");
            Czy_mozna_wykonac_ruch = true;
        }
        else if((!szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().equals(" ")) && (szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals(kolor))) {
            System.out.println("Mozna wykonac ruch - Mamy bicie!");
            Czy_jest_szach = true;
            Czy_mozna_wykonac_ruch = true;
        }
        else System.out.println("Nie mozna wykonac ruchu - Podane pole jest zajete przez figure tego samego koloru");
    }

    //Metoda sprawdzajaca mozliwosci ruchu dla gonca
    private static void Warunek_goniec(String kolor)
    {
        if((szachownica[wiersz_figury][kolumna_figury].getNazwa_figury().charAt(1)+"").equals("G"))
            System.out.println("\nSprawdzamy warunek dla gonca:");
        int ilosc_kolumn_do_przejscia = kolumna_docelowa - kolumna_figury;
        int ilosc_wierszy_do_przejscia = wiersz_docelowy - wiersz_figury;
        System.out.println("Ilosc pol do przejscia: " + (Math.abs(ilosc_kolumn_do_przejscia)));
        boolean flaga = false;
        int w,k;
        //Ruch w prawa strone i do gory (na ukos)
        if (((ilosc_kolumn_do_przejscia > 0) && (ilosc_wierszy_do_przejscia < 0)) && (ilosc_kolumn_do_przejscia == Math.abs(ilosc_wierszy_do_przejscia))){
            System.out.println("Przesuwamy sie w kierunku prawego, gornego rogu");
            for (w = 1; w <= Math.abs(ilosc_wierszy_do_przejscia); w++) {
                for (k = 1; k <= ilosc_kolumn_do_przejscia; k++) {
                    if (szachownica[wiersz_figury - w][kolumna_figury + k].getNazwa_figury().equals(" "))
                    {
                        flaga = true;
                    }else {
                        if (((Math.abs(ilosc_wierszy_do_przejscia) - w) == 0) && ((ilosc_kolumn_do_przejscia - w) == 0))
                            if(szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if (((Math.abs(ilosc_kolumn_do_przejscia) - w) > 0)) {
                            flaga = false;
                            break;
                        }
                    }
                }
                if (!szachownica[wiersz_figury - w][kolumna_figury + w].getNazwa_figury().equals(" "))
                    if (((Math.abs(ilosc_wierszy_do_przejscia) - w) > 0)
                    ) {
                        flaga = false;
                        break;
                    }
            }
            if (flaga) {
                System.out.println("Mozna wykonac ruch");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else
                System.out.println("Nie mozna wykonac ruch!");
        }
        //Ruch w prawa strone i do dolu (na ukos)
        else if (((ilosc_kolumn_do_przejscia > 0) && (ilosc_wierszy_do_przejscia > 0)) && (ilosc_kolumn_do_przejscia == ilosc_wierszy_do_przejscia)) {
            System.out.println("Przesuwamy sie w kierunku prawego, dolnego rogu");
            for (w = 1; w <= ilosc_wierszy_do_przejscia; w++) {
                for (k = 1; k <= ilosc_kolumn_do_przejscia; k++) {
                    if (szachownica[wiersz_figury + w][kolumna_figury + k].getNazwa_figury().equals(" "))
                    {
                        flaga = true;
                    }else {
                        if (((ilosc_wierszy_do_przejscia - w) == 0) && ((ilosc_kolumn_do_przejscia - w) == 0))
                            if(szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if (((ilosc_kolumn_do_przejscia - w) > 0)) {
                            flaga = false;
                            break;
                        }
                    }
                }
                if (!szachownica[wiersz_figury + w][kolumna_figury + w].getNazwa_figury().equals(" "))
                    if (((Math.abs(ilosc_wierszy_do_przejscia) - w) > 0)
                    ) {
                        flaga = false;
                        break;
                    }
            }
            if (flaga) {
                System.out.println("Mozna wykonac ruch");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else
                System.out.println("Nie mozna wykonac ruch!");
        }
        //Ruch w lewa strone i do gory (na ukos)
        else if (((ilosc_kolumn_do_przejscia < 0) && (ilosc_wierszy_do_przejscia < 0)) && (ilosc_kolumn_do_przejscia == ilosc_wierszy_do_przejscia)) {
            System.out.println("Przesuwamy sie w kierunku lewego, gornego rogu");
            for (w = 1; w <= Math.abs(ilosc_wierszy_do_przejscia); w++) {
                for (k = 1; k <= Math.abs(ilosc_kolumn_do_przejscia); k++) {
                    if (szachownica[wiersz_figury - w][kolumna_figury - k].getNazwa_figury().equals(" "))
                    {
                        flaga = true;
                    }else {
                        if (((Math.abs(ilosc_wierszy_do_przejscia) - w) == 0) && ((Math.abs(ilosc_kolumn_do_przejscia) - w) == 0))
                            if(szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if (((Math.abs(ilosc_kolumn_do_przejscia) - w) > 0)) {
                            flaga = false;
                            break;
                        }
                    }
                }
                if (!szachownica[wiersz_figury - w][kolumna_figury - w].getNazwa_figury().equals(" "))
                    if (((Math.abs(ilosc_wierszy_do_przejscia) - w) > 0)
                    ) {
                        flaga = false;
                        break;
                    }
            }
            if (flaga) {
                System.out.println("Mozna wykonac ruch");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else
                System.out.println("Nie mozna wykonac ruch!");
        }
        //Ruch w lewa strone i do dolu (na ukos)
        else if (((ilosc_kolumn_do_przejscia < 0) && (ilosc_wierszy_do_przejscia > 0)) && (Math.abs(ilosc_kolumn_do_przejscia) == ilosc_wierszy_do_przejscia)) {
            System.out.println("Przesuwamy sie w kierunku lewego, dolnego rogu");
            for (w = 1; w <= ilosc_wierszy_do_przejscia; w++) {
                for (k = 1; k <= Math.abs(ilosc_kolumn_do_przejscia); k++) {
                    if (szachownica[wiersz_figury + w][kolumna_figury - k].getNazwa_figury().equals(" "))
                    {
                        flaga = true;
                    }else {
                        if (((ilosc_wierszy_do_przejscia - w) == 0) && ((Math.abs(ilosc_kolumn_do_przejscia) - w) == 0))
                            if(szachownica[wiersz_docelowy][kolumna_docelowa].getKolor().equals(kolor))
                                flaga = true;
                            else
                                flaga = false;
                        else if (((Math.abs(ilosc_kolumn_do_przejscia) - w) > 0)) {
                            flaga = false;
                            break;
                        }
                    }
                }
                if (!szachownica[wiersz_figury + w][kolumna_figury - w].getNazwa_figury().equals(" "))
                    if (((Math.abs(ilosc_wierszy_do_przejscia) - w) > 0)
                    ) {
                        flaga = false;
                        break;
                    }
            }
            if (flaga) {
                System.out.println("Mozna wykonac ruch");
                Czy_jest_szach = true;
                Czy_mozna_wykonac_ruch = true;
            }
            else
                System.out.println("Nie mozna wykonac ruch!");
        }
        else {
            if (szachownica[wiersz_figury][kolumna_figury].getNazwa_figury().equals("BG") || szachownica[wiersz_figury][kolumna_figury].getNazwa_figury().equals("CG"))
                System.out.println("Goniec nie moze wykonac takiego ruchu.");
            else if (szachownica[wiersz_figury][kolumna_figury].getNazwa_figury().equals("BQ")|| szachownica[wiersz_figury][kolumna_figury].getNazwa_figury().equals("CQ"))
                System.out.println("Krolowa nie moze wykonac takiego ruchu.");
        }
    }

    //Metoda sprawdzajaca mozliwosci ruchu dla krolowej
    private static void Warunek_krolowa(String kolor)
    {
        System.out.println("\nSprawdzamy warunek dla krolowej:");
        if (((kolumna_figury - kolumna_docelowa) != 0) && (wiersz_figury - wiersz_docelowy) == 0) {
            Warunek_wieza(kolor);
        }
        else if (((wiersz_figury - wiersz_docelowy) != 0) && (kolumna_figury - kolumna_docelowa) == 0) {
            Warunek_wieza(kolor);
        }
        else {
            if (((kolumna_docelowa - kolumna_figury) > 0) && (wiersz_docelowy - wiersz_figury) < 0){
                Warunek_goniec(kolor);
            }
            else if (((kolumna_docelowa - kolumna_figury) > 0) && (wiersz_docelowy - wiersz_figury) > 0){
                Warunek_goniec(kolor);
            }
            else if (((kolumna_docelowa - kolumna_figury) < 0) && (wiersz_docelowy - wiersz_figury) < 0){
                Warunek_goniec(kolor);
            }
            else if (((kolumna_docelowa - kolumna_figury) < 0) && (wiersz_docelowy - wiersz_figury) > 0){
                Warunek_goniec(kolor);
            }
            else System.out.println("Krolowa nie moze wykonac takiego ruchu.");
        }
    }

    //Metoda sprawdzajaca mozliwosci ruchu dla krola
    private static void Warunek_krol(String kolor)
    {
        System.out.println("\nSprawdzamy warunek dla krola:");
        //Ruch gora
        if(((kolumna_docelowa - kolumna_figury) == 0) && ((wiersz_docelowy - wiersz_figury) == -1))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch dol
        else if(((kolumna_docelowa - kolumna_figury) == 0) && ((wiersz_docelowy - wiersz_figury) == 1))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch w prawo
        else if(((kolumna_docelowa - kolumna_figury) == 1) && ((wiersz_docelowy - wiersz_figury) == 0))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch lewo
        else if(((kolumna_docelowa - kolumna_figury) == -1) && ((wiersz_docelowy - wiersz_figury) == 0))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch prawy gorny ukos
        else if(((kolumna_docelowa - kolumna_figury) == 1) && ((wiersz_docelowy - wiersz_figury) == -1))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch prawy dolny ukos
        else if(((kolumna_docelowa - kolumna_figury) == 1) && ((wiersz_docelowy - wiersz_figury) == 1))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch lewy gorny ukos
        else if(((kolumna_docelowa - kolumna_figury) == -1) && ((wiersz_docelowy - wiersz_figury) == -1))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        //Ruch lewy dolny ukos
        else if(((kolumna_docelowa - kolumna_figury) == -1) && ((wiersz_docelowy - wiersz_figury) == 1))
        {
            Czy_mozna_wykonac_ruch(kolor);
        }
        else System.out.println("Krol nie moze wykonac takiego ruchu!");
    }

    //Metoda sprawdzajaca czy po wykonaniu ruchu nasz krol nie bedzie mial mata
    public static void Czy_krol_nie_bedzie_szachowany()
    {
        //Przesuwanie wybranej figury na miejsce docelowe
        szachownica[wiersz_docelowy][kolumna_docelowa].setNazwa_figury(szachownica[wiersz_figury][kolumna_figury].getNazwa_figury());
        szachownica[wiersz_figury][kolumna_figury].setNazwa_figury(" ");

        System.out.println("\nAktualna szachwonica: \n");
        Rysowanie_szachownicy();
        int ilosc_figur_do_sprawdzenia = 0;
        int wiersz_naszego_krola = 0;
        int kolumna_naszego_krola = 0;

        //Sprawdzamy lokalizacje naszego krola
        System.out.println("\nSprawdzamy czy ruch odslania naszego krola. \n");
        for(int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if((szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().charAt(0)+"").equals("C"))
                {
                    if((szachownica[i][j].getNazwa_figury().charAt(0)+"").equals("B"))
                        ilosc_figur_do_sprawdzenia++;

                    if(szachownica[i][j].getNazwa_figury().equals("CK"))
                    {
                        wiersz_naszego_krola = i;
                        kolumna_naszego_krola = j;
                    }
                }
                else if((szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().charAt(0)+"").equals("B"))
                {
                    if((szachownica[i][j].getNazwa_figury().charAt(0)+"").equals("C"))
                        ilosc_figur_do_sprawdzenia++;

                    if(szachownica[i][j].getNazwa_figury().equals("BK"))
                    {
                        wiersz_naszego_krola = i;
                        kolumna_naszego_krola = j;
                    }
                }
            }
        }
        System.out.println("Ilosc figur do sprawdzenia: " + ilosc_figur_do_sprawdzenia);
        System.out.println("Pozycja naszego krola to: "  + szachownica[wiersz_naszego_krola][kolumna_naszego_krola].getPole_znak() + szachownica[wiersz_naszego_krola][kolumna_naszego_krola].getPole_cyfra());
        kolumna_docelowa = kolumna_naszego_krola;
        wiersz_docelowy = wiersz_naszego_krola;
        int licznik = 1;
        Czy_jest_szach = false;

        //Sprawdzamy czy jesli mozemy wykonac ruch to czy przypadkiem dowolna figura przeciwnika nie bedzie w stanie szachowac naszego krola
        for(int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if((szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().charAt(0)+"").equals("C"))
                {
                    kolumna_figury = j;
                    wiersz_figury = i;
                    switch ((szachownica[i][j].getNazwa_figury())) {
                        case "BP":
                            System.out.print("\nFigura nr " + licznik + ": Bialy pionek.");
                            Warunek_pionek();
                            licznik++;
                            break;
                        case "BW":
                            System.out.print("\nFigura nr " + licznik + ": Biala wieza.");
                            Warunek_wieza("Czarny");
                            licznik++;
                            break;
                        case "BS":
                            System.out.print("\nFigura nr " + licznik + ": Bialy skoczek.");
                            Warunek_skoczek("Czarny");
                            licznik++;
                            break;
                        case "BG":
                            System.out.print("\nFigura nr " + licznik + ": Bialy goniec.");
                            Warunek_goniec("Czarny");
                            licznik++;
                            break;
                        case "BK":
                            System.out.print("\nFigura nr " + licznik + ": Bialy krol.");
                            Warunek_krol("Czarny");
                            licznik++;
                            break;
                        case "BQ":
                            System.out.print("\nFigura nr " + licznik + ": Biala krolowa.");
                            Warunek_krolowa("Czarny");
                            licznik++;
                            break;
                    }
                }
                else if((szachownica[wiersz_docelowy][kolumna_docelowa].getNazwa_figury().charAt(0)+"").equals("B"))
                {
                    kolumna_figury = j;
                    wiersz_figury = i;
                    switch ((szachownica[i][j].getNazwa_figury())) {
                        case "CP":
                            System.out.print("\nFigura nr " + licznik + ": Czarny pionek.");
                            Warunek_pionek();
                            licznik++;
                            break;
                        case "CW":
                            System.out.print("\nFigura nr " + licznik + ": Czarna wieza.");
                            Warunek_wieza("Bialy");
                            licznik++;
                            break;
                        case "CS":
                            System.out.print("\nFigura nr " + licznik + ": Czarny skoczek.");
                            Warunek_skoczek("Bialy");
                            licznik++;
                            break;
                        case "CG":
                            System.out.print("\nFigura nr " + licznik + ": Czarny goniec.");
                            Warunek_goniec("Bialy");
                            licznik++;
                            break;
                        case "CK":
                            System.out.print("\nFigura nr " + licznik + ": Czarny krol.");
                            Warunek_krol("Bialy");
                            licznik++;
                            break;
                        case "CQ":
                            System.out.print("\nFigura nr " + licznik + ": Czarna krolowa.");
                            Warunek_krolowa("Bialy");
                            licznik++;
                            break;
                    }
                }
                if(Czy_jest_szach)
                    break;
            }
            if(Czy_jest_szach)
                break;
        }

        if(!Czy_jest_szach)
            System.out.println("\n\nPo wykonaniu ruchu nasz krol jest bezpieczny, wiec mozemy go wykonac.");
        else
            System.out.println("\n\nNie mozemy wykonac naszego ruchu, poniewaz po jego wykonaniu nasz krol bedzie szachowany!");
    }



}
